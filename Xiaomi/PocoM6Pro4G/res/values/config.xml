<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- Whether the display blanks itself when transition from a doze to a non-doze state -->
    <bool name="config_displayBlanksAfterDoze">false</bool>

    <!-- Indicate whether closing the lid causes the device to go to sleep and opening
         it causes the device to wake up.
         The default is false. -->
    <bool name="config_lidControlsSleep">true</bool>

    <!-- Power Management: Specifies whether to decouple the auto-suspend state of the
         device from the display on/off state.
         When false, autosuspend_disable() will be called before the display is turned on
         and autosuspend_enable() will be called after the display is turned off.
         This mode provides best compatibility for devices using legacy power management
         features such as early suspend / late resume.
         When true, autosuspend_display() and autosuspend_enable() will be called
         independently of whether the display is being turned on or off.  This mode
         enables the power manager to suspend the application processor while the
         display is on.
         This resource should be set to "true" when a doze component has been specified
         to maximize power savings but not all devices support it.
         Refer to autosuspend.h for details. -->
    <bool name="config_powerDecoupleAutoSuspendModeFromDisplay">true</bool>

    <!-- Power Management: Specifies whether to decouple the interactive state of the
         device from the display on/off state.
         When false, setInteractive(..., true) will be called before the display is turned on
         and setInteractive(..., false) will be called after the display is turned off.
         This mode provides best compatibility for devices that expect the interactive
         state to be tied to the display state.
         When true, setInteractive(...) will be called independently of whether the display
         is being turned on or off.  This mode enables the power manager to reduce
         clocks and disable the touch controller while the display is on.
         This resource should be set to "true" when a doze component has been specified
         to maximize power savings but not all devices support it.
         Refer to power.h for details. -->
    <bool name="config_powerDecoupleInteractiveModeFromDisplay">true</bool>

    <!-- Whether a software navigation bar should be shown. -->
    <bool name="config_showNavigationBar">true</bool>

    <!-- Whether the display cutout region of the main built-in display should be forced to
         black in software (to avoid aliasing or emulate a cutout that is not physically existent).
         -->
    <bool name="config_fillMainBuiltInDisplayCutout">false</bool>

    <!-- When true use the linux /dev/input/event subsystem to detect the switch changes
         on the headphone/microphone jack. When false use the older uevent framework. -->
    <bool name="config_useDevInputEventForAudioJack">true</bool>

    <!-- Control whether the always on display mode is available. This should only be enabled on
         devices where the display has been tuned to be power efficient in DOZE and/or DOZE_SUSPEND
         states. -->
    <bool name="config_dozeAlwaysOnDisplayAvailable">true</bool>

    <!-- If true, the display will be shifted around in ambient mode. -->
    <bool name="config_enableBurnInProtection">true</bool>

    <!-- Stability requirements in milliseconds for accepting a new brightness level.  This is used
         for debouncing the light sensor.  Different constants are used to debounce the light sensor
         when adapting to brighter or darker environments.  This parameter controls how quickly
         brightness changes occur in response to an observed change in light level that exceeds the
         hysteresis threshold. -->
    <integer name="config_autoBrightnessBrighteningLightDebounce">1000</integer>
    <integer name="config_autoBrightnessDarkeningLightDebounce">1000</integer>

    <!-- Fast brightness animation ramp rate in brightness units per second-->
    <integer name="config_brightness_ramp_rate_fast">4932</integer>

    <!-- Slow brightness animation ramp rate in brightness units per second-->
    <integer name="config_brightness_ramp_rate_slow">3946</integer>

    <!-- The default refresh rate for a given device. Change this value to set a higher default
         refresh rate. If the hardware composer on the device supports display modes with a higher
         refresh rate than the default value specified here, the framework may use those higher
         refresh rate modes if an app chooses one by setting preferredDisplayModeId or calling
         setFrameRate().
         If a non-zero value is set for config_defaultPeakRefreshRate, then
         config_defaultRefreshRate may be set to 0, in which case the value set for
         config_defaultPeakRefreshRate will act as the default frame rate. -->
    <integer name="config_defaultRefreshRate">121</integer>

    <!-- The default peak refresh rate for a given device. Change this value if you want to prevent
         the framework from using higher refresh rates, even if display modes with higher refresh
         rates are available from hardware composer. Only has an effect if the value is
         non-zero. -->
    <integer name="config_defaultPeakRefreshRate">121</integer>

    <!-- Screen brightness used to dim the screen when the user activity
         timeout expires.  May be less than the minimum allowed brightness setting
         that can be set by the user. -->
    <integer name="config_screenBrightnessDim">8</integer>

    <!-- Default screen brightness setting.
         Must be in the range specified by minimum and maximum. -->
    <integer name="config_screenBrightnessSettingDefault">307</integer>

    <!-- Maximum screen brightness setting allowed by the power manager.
         The user is forbidden from setting the brightness above this level. -->
    <integer name="config_screenBrightnessSettingMaximum">4095</integer>

    <!-- Screen brightness used to dim the screen while dozing in a very low power state.
         May be less than the minimum allowed brightness setting
         that can be set by the user. -->
    <integer name="config_screenBrightnessDoze">5</integer>

    <!-- Minimum screen brightness setting allowed by the power manager.
         The user is forbidden from setting the brightness below this level. -->
    <integer name="config_screenBrightnessSettingMinimum">8</integer>

    <!-- Shutdown if the battery temperature exceeds (this value * 0.1) Celsius. -->
    <integer name="config_shutdownBatteryTemperature">580</integer>

    <!-- Array of hysteresis constraint values for brightening, represented as tenths of a
         percent. The length of this array is assumed to be one greater than
         config_ambientThresholdLevels. The brightening threshold is calculated as
         lux * (1.0f + CONSTRAINT_VALUE). When the current lux is higher than this threshold,
         the screen brightness is recalculated. See the config_ambientThresholdLevels
         description for how the constraint value is chosen. -->
    <integer-array name="config_ambientBrighteningThresholds">
        <item>5</item>
        <item>6</item>
        <item>10</item>
        <item>30</item>
        <item>100</item>
        <item>400</item>
        <item>600</item>
        <item>1000</item>
    </integer-array>

    <!-- Array of hysteresis constraint values for darkening, represented as tenths of a
         percent. The length of this array is assumed to be one greater than
         config_ambientThresholdLevels. The darkening threshold is calculated as
         lux * (1.0f - CONSTRAINT_VALUE). When the current lux is lower than this threshold,
         the screen brightness is recalculated. See the config_ambientThresholdLevels
         description for how the constraint value is chosen. -->
    <integer-array name="config_ambientDarkeningThresholds">
        <item>800</item>
        <item>650</item>
        <item>500</item>
        <item>500</item>
        <item>500</item>
        <item>500</item>
        <item>500</item>
        <item>500</item>
    </integer-array>

    <!-- Array of ambient lux threshold values. This is used for determining hysteresis constraint
         values by calculating the index to use for lookup and then setting the constraint value
         to the corresponding value of the array. The new brightening hysteresis constraint value
         is the n-th element of config_ambientBrighteningThresholds, and the new darkening
         hysteresis constraint value is the n-th element of config_ambientDarkeningThresholds.
         The (zero-based) index is calculated as follows: (MAX is the largest index of the array)
         condition                       calculated index
         value < level[0]                0
         level[n] <= value < level[n+1]  n+1
         level[MAX] <= value             MAX+1 -->
    <integer-array name="config_ambientThresholdLevels">
        <item>2</item>
        <item>10</item>
        <item>30</item>
        <item>100</item>
        <item>800</item>
        <item>2000</item>
        <item>4000</item>
    </integer-array>

    <!-- Array of desired screen brightness in nits corresponding to the lux values
         in the config_autoBrightnessLevels array. As with config_screenBrightnessMinimumNits and
         config_screenBrightnessMaximumNits, the display brightness is defined as the measured
         brightness of an all-white image.
         If this is defined then:
            - config_autoBrightnessLcdBacklightValues should not be defined
            - config_screenBrightnessNits must be defined
            - config_screenBrightnessBacklight must be defined
         This array should have size one greater than the size of the config_autoBrightnessLevels
         array. The brightness values must be non-negative and non-decreasing. This must be
         overridden in platform specific overlays -->
    <array name="config_autoBrightnessDisplayValuesNits">
        <item>3.8</item>
        <item>7.7</item>
        <item>11.9</item>
        <item>16.1</item>
        <item>22.2</item>
        <item>26.4</item>
        <item>29.6</item>
        <item>30.8</item>
        <item>32.1</item>
        <item>33.5</item>
        <item>35.0</item>
        <item>36.6</item>
        <item>38.2</item>
        <item>40.0</item>
        <item>41.8</item>
        <item>43.7</item>
        <item>45.8</item>
        <item>47.9</item>
        <item>50.0</item>
        <item>53.3</item>
        <item>56.3</item>
        <item>57.1</item>
        <item>59.7</item>
        <item>62.3</item>
        <item>65.8</item>
        <item>68.8</item>
        <item>70.7</item>
        <item>73.7</item>
        <item>77.7</item>
        <item>81.7</item>
        <item>84.1</item>
        <item>84.3</item>
        <item>84.5</item>
        <item>84.5</item>
        <item>85.0</item>
        <item>85.4</item>
        <item>85.8</item>
        <item>86.2</item>
        <item>86.7</item>
        <item>87.1</item>
        <item>87.6</item>
        <item>88.0</item>
        <item>88.5</item>
        <item>88.9</item>
        <item>89.4</item>
        <item>90.8</item>
        <item>92.3</item>
        <item>93.7</item>
        <item>95.2</item>
        <item>96.6</item>
        <item>98.1</item>
        <item>99.6</item>
        <item>101.0</item>
        <item>102.5</item>
        <item>103.9</item>
        <item>105.4</item>
        <item>106.8</item>
        <item>108.2</item>
        <item>109.7</item>
        <item>111.3</item>
        <item>112.7</item>
        <item>114.1</item>
        <item>115.6</item>
        <item>117.1</item>
        <item>118.6</item>
        <item>126.1</item>
        <item>135.8</item>
        <item>155.1</item>
        <item>175.3</item>
        <item>196.8</item>
        <item>218.7</item>
        <item>241.1</item>
        <item>263.8</item>
        <item>278.4</item>
        <item>335.4</item>
        <item>397.9</item>
        <item>415.7</item>
        <item>433.4</item>
        <item>451.1</item>
        <item>468.8</item>
        <item>486.5</item>
        <item>504.2</item>
        <item>521.9</item>
        <item>539.6</item>
        <item>557.3</item>
        <item>575.0</item>
        <item>592.8</item>
        <item>610.5</item>
        <item>628.2</item>
        <item>645.9</item>
        <item>663.6</item>
        <item>681.3</item>
        <item>699.0</item>
        <item>713.7</item>
        <item>734.4</item>
        <item>752.1</item>
        <item>769.8</item>
        <item>787.5</item>
        <item>805.2</item>
        <item>822.9</item>
        <item>858.3</item>
        <item>893.8</item>
        <item>929.1</item>
        <item>964.6</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
        <item>1000.0</item>
    </array>

    <!-- Array of output values for LCD backlight corresponding to the LUX values
         in the config_autoBrightnessLevels array.  This array should have size one greater
         than the size of the config_autoBrightnessLevels array.
         This must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLcdBacklightValues">
        <item>13</item>
        <item>30</item>
        <item>48</item>
        <item>65</item>
        <item>82</item>
        <item>99</item>
        <item>116</item>
        <item>125</item>
        <item>130</item>
        <item>136</item>
        <item>142</item>
        <item>149</item>
        <item>155</item>
        <item>163</item>
        <item>170</item>
        <item>178</item>
        <item>186</item>
        <item>195</item>
        <item>204</item>
        <item>213</item>
        <item>225</item>
        <item>233</item>
        <item>243</item>
        <item>254</item>
        <item>268</item>
        <item>276</item>
        <item>288</item>
        <item>301</item>
        <item>313</item>
        <item>326</item>
        <item>339</item>
        <item>341</item>
        <item>343</item>
        <item>345</item>
        <item>347</item>
        <item>349</item>
        <item>350</item>
        <item>352</item>
        <item>354</item>
        <item>355</item>
        <item>357</item>
        <item>359</item>
        <item>361</item>
        <item>363</item>
        <item>365</item>
        <item>371</item>
        <item>377</item>
        <item>383</item>
        <item>389</item>
        <item>394</item>
        <item>401</item>
        <item>407</item>
        <item>412</item>
        <item>419</item>
        <item>424</item>
        <item>430</item>
        <item>436</item>
        <item>442</item>
        <item>448</item>
        <item>455</item>
        <item>460</item>
        <item>466</item>
        <item>472</item>
        <item>478</item>
        <item>484</item>
        <item>515</item>
        <item>555</item>
        <item>634</item>
        <item>717</item>
        <item>805</item>
        <item>894</item>
        <item>986</item>
        <item>1079</item>
        <item>1126</item>
        <item>1372</item>
        <item>1628</item>
        <item>1701</item>
        <item>1774</item>
        <item>1846</item>
        <item>1918</item>
        <item>1991</item>
        <item>2064</item>
        <item>2136</item>
        <item>2208</item>
        <item>2281</item>
        <item>2353</item>
        <item>2426</item>
        <item>2499</item>
        <item>2571</item>
        <item>2644</item>
        <item>2716</item>
        <item>2789</item>
        <item>2861</item>
        <item>2934</item>
        <item>3006</item>
        <item>3079</item>
        <item>3151</item>
        <item>3224</item>
        <item>3296</item>
        <item>3369</item>
        <item>3514</item>
        <item>3659</item>
        <item>3803</item>
        <item>3949</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
        <item>4095</item>
    </integer-array>

    <!-- Array of light sensor LUX values to define our levels for auto backlight brightness support.
         The N entries of this array define N  1 zones as follows:

         Zone 0:        0 <= LUX < array[0]
         Zone 1:        array[0] <= LUX < array[1]
         ...
         Zone N:        array[N - 1] <= LUX < array[N]
         Zone N + 1     array[N] <= LUX < infinity

         Must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLevels">
        <item>1</item>
        <item>2</item>
        <item>3</item>
        <item>4</item>
        <item>5</item>
        <item>6</item>
        <item>7</item>
        <item>8</item>
        <item>9</item>
        <item>10</item>
        <item>11</item>
        <item>12</item>
        <item>13</item>
        <item>14</item>
        <item>15</item>
        <item>16</item>
        <item>17</item>
        <item>18</item>
        <item>19</item>
        <item>20</item>
        <item>21</item>
        <item>22</item>
        <item>23</item>
        <item>24</item>
        <item>25</item>
        <item>26</item>
        <item>27</item>
        <item>28</item>
        <item>29</item>
        <item>30</item>
        <item>35</item>
        <item>40</item>
        <item>45</item>
        <item>50</item>
        <item>55</item>
        <item>60</item>
        <item>65</item>
        <item>70</item>
        <item>75</item>
        <item>80</item>
        <item>85</item>
        <item>90</item>
        <item>95</item>
        <item>100</item>
        <item>120</item>
        <item>140</item>
        <item>160</item>
        <item>180</item>
        <item>200</item>
        <item>220</item>
        <item>240</item>
        <item>260</item>
        <item>280</item>
        <item>300</item>
        <item>320</item>
        <item>340</item>
        <item>360</item>
        <item>380</item>
        <item>400</item>
        <item>420</item>
        <item>440</item>
        <item>460</item>
        <item>480</item>
        <item>500</item>
        <item>600</item>
        <item>700</item>
        <item>900</item>
        <item>1100</item>
        <item>1300</item>
        <item>1500</item>
        <item>1700</item>
        <item>1900</item>
        <item>2000</item>
        <item>2500</item>
        <item>3000</item>
        <item>3500</item>
        <item>4000</item>
        <item>4500</item>
        <item>5000</item>
        <item>5500</item>
        <item>6000</item>
        <item>6500</item>
        <item>7000</item>
        <item>7500</item>
        <item>8000</item>
        <item>8500</item>
        <item>9000</item>
        <item>9500</item>
        <item>10000</item>
        <item>10500</item>
        <item>11000</item>
        <item>11500</item>
        <item>12000</item>
        <item>12500</item>
        <item>13000</item>
        <item>13500</item>
        <item>14000</item>
        <item>14500</item>
        <item>15000</item>
        <item>16000</item>
        <item>17000</item>
        <item>18000</item>
        <item>19000</item>
        <item>20000</item>
        <item>21000</item>
        <item>22000</item>
        <item>23000</item>
        <item>24000</item>
        <item>25000</item>
        <item>26000</item>
        <item>27000</item>
        <item>28000</item>
        <item>29000</item>
        <item>30000</item>
        <item>35000</item>
        <item>40000</item>
        <item>45000</item>
        <item>50000</item>
        <item>55000</item>
        <item>60000</item>
        <item>65000</item>
        <item>70000</item>
        <item>75000</item>
        <item>80000</item>
        <item>85000</item>
        <item>90000</item>
        <item>95000</item>
        <item>100000</item>
    </integer-array>

    <!-- Array of hysteresis constraint values for brightening, represented as tenths of a
         percent. The length of this array is assumed to be one greater than
         config_screenThresholdLevels. The brightening threshold is calculated as
         screenBrightness * (1.0f + CONSTRAINT_VALUE). When the new screen brightness is higher
         than this threshold, it is applied. See the config_screenThresholdLevels description for
         how the constraint value is chosen. -->
    <integer-array name="config_screenBrighteningThresholds">
        <item>0</item>
    </integer-array>

    <!-- Array of hysteresis constraint values for darkening, represented as tenths of a
         percent. The length of this array is assumed to be one greater than
         config_screenThresholdLevels. The darkening threshold is calculated as
         screenBrightness * (1.0f - CONSTRAINT_VALUE). When the new screen brightness is lower than
         this threshold, it is applied. See the config_screenThresholdLevels description for how
         the constraint value is chosen. -->
    <integer-array name="config_screenDarkeningThresholds">
        <item>0</item>
    </integer-array>

    <!-- List supported color modes. -->
    <integer-array name="config_availableColorModes">
        <item>0</item> <!-- COLOR_MODE_NATURAL -->
        <item>7</item> <!-- COLOR_MODE_BOOSTED -->
        <item>9</item> <!-- COLOR_MODE_AUTOMATIC -->
    </integer-array>

    <!-- Default screen brightness setting
         Must be in the range specified by minimum and maximum. -->
    <item type="dimen" name="config_screenBrightnessSettingDefaultFloat">0.074743524</item>

    <!-- Maximum screen brightness allowed by the power manager.
         The user is forbidden from setting the brightness above this level. -->
    <item type="dimen" name="config_screenBrightnessSettingMaximumFloat">1.0</item>

    <!-- Minimum screen brightness setting allowed by power manager.
         The user is forbidden from setting the brightness below this level.  -->
    <item type="dimen" name="config_screenBrightnessSettingMinimumFloat">0.001709819</item>

    <!-- Radius of the software rounded corners. -->
    <dimen name="rounded_corner_radius">70.0px</dimen>

    <!-- Radius of the software rounded corners at the bottom of the display in its natural
        orientation. If zero, the value of rounded_corner_radius is used. -->
    <dimen name="rounded_corner_radius_bottom">70.0px</dimen>

    <!-- Radius of the software rounded corners at the top of the display in its natural
        orientation. If zero, the value of rounded_corner_radius is used. -->
    <dimen name="rounded_corner_radius_top">70.0px</dimen>

    <!-- Height of the status bar -->
    <dimen name="status_bar_height_default">94.0px</dimen>
    <dimen name="status_bar_height_portrait">94.0px</dimen>
    <dimen name="status_bar_height_landscape">28.0dip</dimen>

    <!-- The maximum range of gamma adjustment possible using the screen
         auto-brightness adjustment setting. -->
    <fraction name="config_autoBrightnessAdjustmentMaxGamma">100.0%</fraction>

    <!-- The bounding path of the cutout region of the main built-in display.
         Must either be empty if there is no cutout region, or a string that is parsable by
         {@link android.util.PathParser}.
         The path is assumed to be specified in display coordinates with pixel units and in
         the display's native orientation, with the origin of the coordinate system at the
         center top of the display.
         To facilitate writing device-independent emulation overlays, the marker `@dp` can be
         appended after the path string to interpret coordinates in dp instead of px units.
         Note that a physical cutout should be configured in pixels for the best results.
         -->
    <string name="config_mainBuiltInDisplayCutout">M 0,0 H -28 V 94 H 28 V 0 H 0 Z</string>

    <!-- Like config_mainBuiltInDisplayCutout, but this path is used to report the
         one single bounding rect per device edge to the app via
         {@link DisplayCutout#getBoundingRect}. Note that this path should try to match the visual
         appearance of the cutout as much as possible, and may be smaller than
         config_mainBuiltInDisplayCutout
         -->
    <string name="config_mainBuiltInDisplayCutoutRectApproximation">M 0,0 H -28 V 94 H 28 V 0 H 0 Z</string>

    <!-- List of biometric sensors on the device, in decreasing strength. Consumed by AuthService
         when registering authenticators with BiometricService. Format must be ID:Modality:Strength,
         and Strength as defined in Authenticators.java -->
    <string-array name="config_biometric_sensors">
        <item>0:2:15</item>
    </string-array>
</resources>
